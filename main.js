require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const morgan = require('morgan');
const swaggerUi = require('swagger-ui-express');
const v1Routes = require('./src/routers/v1-base.router');
const doc = require('./doc/api.doc.json');

const app = express();

app.use(morgan("tiny"));
app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended : true}));


app.use('/api/v1',v1Routes);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(doc));
app.use(function(req, res, next) {
  return res.status(200).send({ app_name : "Stack Overflow",version : "v1"});
});



var port = process.env.PORT || 3000
app.listen(port);
console.log(`Now listening on PORT: ${port}`);