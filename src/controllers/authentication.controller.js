const Response = require("../extensions/response.extension");
const Helper = require("../extensions/helpers.extension");
const UserServices = require("../services/user.service");
const AuthService = require("../services/authentication.service");

class AuthenticationController{

    /**
     * user login
     * @param {Express.Request} req 
     * @param {Express.Response} res 
     */
    static async Login(req, res){

        try {

            let errors = Helper.validateInputData(req);
            if(errors){
                return res.status(400).json(
                    Response(true,errors)
                )
            }

            let body = req.body;

            let userId = body.email;

            let user  = await UserServices.findByUniqueId(userId);
            if(!user){
                return res.status(200).json(
                    Response(true,"Invalid Email/password")
                )
            }

            let verifyPwd = Helper.comparePassword(user.password,body.password)
            if(!verifyPwd){
                return res.status(200).json(
                    Response(true,"Invalid Email/password")
                )
            }


            let data = AuthService.generatePayload(user);
            return res.status(200).json(
                Response(false,"sign-in successfully",data)
            )
    

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }

    static async SocialLogin(req,res){
        // todo
    }
}

module.exports = AuthenticationController;