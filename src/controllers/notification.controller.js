const Response = require("../extensions/response.extension");
const Helper = require("../extensions/helpers.extension");
const NotificationServices = require("../services/notification.service");
const AuthMiddleware = require("../middlewares/auth.mw");

class NotificationController {
  
    /**
     * Basic Auth
     */
    static async getUserNotification(req, res){

        try {

            let auth = AuthMiddleware.CheckAuth(req);
            let user = auth.user;
            if(!user){
                return res.status(auth.code).json(
                    Response(true,auth.message)
                ) 
            }

            let queries = req.query;

            user = await NotificationServices.getUserNotification(queries,user);

            return res.status(200).json(
                Response(false,"success",user)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }

    
    
}

module.exports = NotificationController;