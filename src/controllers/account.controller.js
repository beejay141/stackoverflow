const Response = require("../extensions/response.extension");
const Helper = require("../extensions/helpers.extension");
const UserServices = require("../services/user.service");
const AuthMiddleware = require("../middlewares/auth.mw");

class AccountController {
  

    /**
     * 
     * Allow Anonymous 
     * @param {Express.Request} req 
     * @param {Express.Response} res 
     */
    static async Register(req , res){

        try {

            let errors = Helper.validateInputData(req);
            if(errors){
                return res.status(400).json(
                    Response(true,errors)
                )
            }

            let body = req.body;

            let data = await UserServices.registerUser(body);

            return res.status(200).json(
                Response(false,"success",data)
            )
    

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }

    ////////////////////////////////////////////////////////////////////////////


    /**
     * Basic Auth
     */
    static async getBasicProfile(req, res){

        try {

            let auth = AuthMiddleware.CheckAuth(req);
            let user = auth.user;
            if(!user){
                return res.status(auth.code).json(
                    Response(true,auth.message)
                ) 
            }

            user = await UserServices.getBasicProfile(user.email);

            return res.status(200).json(
                Response(false,"success",user)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }

    static async updateBasicProfile(req, res){

        try {

            let auth = AuthMiddleware.CheckAuth(req);
            let user = auth.user;
            if(!user){
                return res.status(auth.code).json(
                    Response(true,auth.message)
                ) 
            }

            let errors = Helper.validateInputData(req);
            if(errors){
                return res.status(400).json(
                    Response(true,errors)
                )
            }

            user = await UserServices.updateBasicProfile(req.body,user);

            return res.status(200).json(
                Response(false,"success",user)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }
    
    
}

module.exports = AccountController;