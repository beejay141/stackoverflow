const Response = require("../extensions/response.extension");
const Helper = require("../extensions/helpers.extension");
const AnswerServices = require("../services/answer.service");
const AuthMiddleware = require("../middlewares/auth.mw");

class AnswerController{

    /**
     * 
     * @param {Express.Request} req 
     * @param {Express.Response} res 
     */
    static async SubmitAnswer(req, res){

        try {

            let auth = AuthMiddleware.CheckAuth(req);
            let user = auth.user;
            if(!user){
                return res.status(auth.code).json(
                    Response(true,auth.message)
                ) 
            }

            let errors = Helper.validateInputData(req);
            if(errors){
                return res.status(400).json(
                    Response(true,errors)
                )
            }

            let body = req.body;


            let data = await AnswerServices.create(body,user);
            return res.status(200).json(
                Response(data.err,data.msg,data.data)
            )
    

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }

}

module.exports = AnswerController;