const Response = require("../extensions/response.extension");
const Helper = require("../extensions/helpers.extension");
const QuestionService = require("../services/question.service");
const AuthMiddleware = require("../middlewares/auth.mw");

class QuestionController {
  

    /**
     * 
     * Allow Anonymous 
     * @param {Express.Request} req 
     * @param {Express.Response} res 
     */
    static async getQuestion(req, res){

        try {

            // let auth = AuthMiddleware.CheckAuth(req);
            // let user = auth.user;
            // if(!user){
            //     return res.status(auth.code).json(
            //         Response(true,auth.message)
            //     ) 
            // }

            let param = req.params;

            let data = await QuestionService.getQuestion(param.id);

            return res.status(200).json(
                Response(data.err,data.msg,data.data)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }

    static async getQuestions(req, res){

        try {

            // let auth = AuthMiddleware.CheckAuth(req);
            // let user = auth.user;
            // if(!user){
            //     return res.status(auth.code).json(
            //         Response(true,auth.message)
            //     ) 
            // }

            let filter = req.query;

            let data = await QuestionService.getQuestions(filter);

            return res.status(200).json(
                Response(false,"success",data)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }
    

    ////////////////////////////////////////////////////////////////////////////


    /**
     * Basic Auth
     */
    static async postQuestion(req, res){

        try {

            let auth = AuthMiddleware.CheckAuth(req);
            let user = auth.user;
            if(!user){
                return res.status(auth.code).json(
                    Response(true,auth.message)
                ) 
            }

            let errors = Helper.validateInputData(req);
            if(errors){
                return res.status(400).json(
                    Response(true,errors)
                )
            }

            let body = req.body;

            let data = await QuestionService.create(body,user);

            return res.status(200).json(
                Response(false,"success",data)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }

    static async EditQuestion(req, res){

        try {

            let auth = AuthMiddleware.CheckAuth(req);
            let user = auth.user;
            if(!user){
                return res.status(auth.code).json(
                    Response(true,auth.message)
                ) 
            }

            let errors = Helper.validateInputData(req);
            if(errors){
                return res.status(400).json(
                    Response(true,errors)
                )
            }

            let body = req.body;
            let param = req.params;

            let data = await QuestionService.editQuestion(param.id,body,user);

            return res.status(200).json(
                Response(data.err,data.msg,data.data)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    }
    
    static async updateVote(req, res){

        try {

            let auth = AuthMiddleware.CheckAuth(req);
            let user = auth.user;
            if(!user){
                return res.status(auth.code).json(
                    Response(true,auth.message)
                ) 
            }

            let errors = Helper.validateInputData(req);
            if(errors){
                return res.status(400).json(
                    Response(true,errors)
                )
            }

            let body = req.body;
            let param = req.params;

            let data = await QuestionService.updateVote(param.id,body,user);

            return res.status(200).json(
                Response(data.err,data.msg,data.data)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    } 

    static async updateSubscribe(req, res){

        try {

            let auth = AuthMiddleware.CheckAuth(req);
            let user = auth.user;
            if(!user){
                return res.status(auth.code).json(
                    Response(true,auth.message)
                ) 
            }

            let errors = Helper.validateInputData(req);
            if(errors){
                return res.status(400).json(
                    Response(true,errors)
                )
            }

            let param = req.params;

            let data = await QuestionService.subscribe(param.id,user);

            return res.status(200).json(
                Response(data.err,data.msg,data.data)
            )

        } catch (error) {

            Helper.logServerError(req,error);

            return res.status(500).json(
                Response(true,"Unexpected Error has Occured, Try again or Contact Support Team")
            )
        }

    } 
    
}

module.exports = QuestionController;