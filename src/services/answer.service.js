const AnswerModel = require('../models/answer.model');
const QuestionService = require('../services/question.service');
const NotificationService = require('./notification.service');

class AnswerService{
    /**
     * 
     * @param {*} data answer data
     * @param {*} qId question id
     * @param {*} auth user submitting the answer
     */
    static async create(data, auth){
        let res ={
            err : false,
            msg : "answer submitted"
        };

        let question = await QuestionService.findOneQuestion(data.questionId);
        if(!question){
            res.err = true;
            res.msg = "question not found";
            return res;
        }

        let ans = {
            question : question._id,
            questionTitle : question.title,
            user : auth._id,
            userName : auth.userName,
            body : data.body
        }
        ans = await AnswerModel.create(ans);

        await QuestionService.updateOne(question._id, {$push : {answers : ans._id},$inc : {answerCount : 1}})
        await NotificationService.notifyUsers(question,ans);
        res.data = ans;
        
        return res;
    }
}

module.exports = AnswerService;