const RoleModel = require('../models/role.model');

class RoleService {

    static async CreateRole(data){
        return await RoleModel.create(data);
    }

    /**
     * get default roles to be assigned to the user 
     * on registration
     */
    static async getDefaultRole(){
        let roles = ["user"]
        return await RoleModel.find({name : {$in : roles}}) || [];
    }

    static async remove(data){
        return await RoleModel.remove(data);
    }

}

module.exports = RoleService;