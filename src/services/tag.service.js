const TagModel = require('../models/tag.model');
class TagService{

    static async create(data){
        return await TagModel.create(data);
    }

    static async remove(data){
        return await TagModel.remove(data);
    }

    static async addTag(data){
        let tag = {
            name : data.name,
            description : data.description
        };

        return await this.create(tag);
    }
}

module.exports = TagService;