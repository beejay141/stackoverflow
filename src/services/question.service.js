const QuestionModel = require("../models/question.model");
const Mapper = require('../extensions/mapper.extension');
const Helper = require('../extensions/helpers.extension');

class QuestionService{

    static async create(data, auth){
        let question = {
            title : data.title,
            body : data.body,
            tags : data.tags,
            user : auth._id,
            userName : auth.userName
        }

        let quest = await QuestionModel.create(question);
        return Mapper.questionVm(quest);
    }

    static async removeQuestion(id, auth){
        let res = {
            err : false,
            msg : "question has been removed"
        }

        let question = await this.findOneQuestion(id,["_id","user"]);
        if(!question){
            res.err = true;
            res.msg = "question not found"
            return res;
        }

        await this.updateOne(question._id, {$set : {deleted : true}});
        return res;
    }

    static async updateOne(data, opt= null){
        if(opt){
            return await QuestionModel.updateOne({_id : data},opt);
        }
        return await QuestionModel.updateOne({_id : data._id},data);
    }

    static async updateMany(data, opt){  
        return await QuestionModel.updateOne({_id : {$in : data}},opt);  
    }

    static async findOneQuestion(id, select = null){
        if(select){
            return await QuestionModel.findOne({_id : id},select).populate(["answers"]);
        }

        return await QuestionModel.findOne({_id:id}).populate(["answers"]);
    }

    static async editQuestion(id, data, auth){
        let res = {
            err : false,
            msg : "question updated"
        }

        let question = await this.findOneQuestion(id);
        if(!question){
            res.err = true;
            res.msg = "Question not found";
            return res;
        }

        question.title = data.title;
        question.body = data.body;
        question.tags = data.tags;
        question.editedBy = auth._id;
        question.editedByUserName = data.userName;
        question.editedOn = new Date();

        await this.updateOne(question);
        res.data = Mapper.questionVm(question);
        return res;
    }

    static async getQuestion(id){
        let res = {
            err : false,
            msg : "success"
        }

        let question = await this.findOneQuestion(id,Mapper.questionVm());
        if(!question){
            res.err = true;
            res.msg = "question not found"
            return res;
        }
        res.data = question;
        return res;
    }

    static async getQuestions(filter){
        
        let queries = this.queryBuilder(filter);

        return await QuestionModel.aggregate(queries);
        
    }

    static queryBuilder(filter){
        let res = [];
        let match = {};
        match.deleted = {$exists : false};

        let query = filter.q;
        if(query){
            let queries = query.split(" ");
            let tags = queries.find(c=>/^\[.*]$/.test(c));
            let users = queries.find(c=>/^user:/.test(c))
            let answers = queries.find(c=>/^answer:/.test(c));
            
            let words = queries.filter(c=>!(/^\[.*]$/.test(c)) && !(/^user:/.test(c))&& !(/^answer:/.test(c)));
            
            
            if(tags){
                let tag = tags.replace("[","").replace("]","");
                match.tags = {$in : [tag]}
            }
            
            if(users){
                let user = users.split(":")[1];
                match.userName = {$regex : user};
            }
            
            if(answers){
                let answer = answers.split(":")[1];
                match.answerCount = {$gte : Number(answer)|| 1}
            }
            
            if(words.length){
                let prject = {tips:{$split : ["$title"," "]},_id : 1,userName : 1,title:1,body:1,votes:1,tags:1,answers:1,answers :1,subscribersCount:1,viewedCount:1,editedByUserName:1,editedOn:1}
                res.push({$project : prject});
                
                match.tips = {$in : words}
            }
        }
        
        res.push({$match : match});

        res.push({$project : {_id : 1,userName : 1,title:1,body:1,votes:1,tags:1,answers:1,answers :1,subscribersCount:1,viewedCount:1,editedByUserName:1,editedOn:1}})

        filter = Helper.setPaging(filter);

        res.push({$skip : filter.skip});
        res.push({$limit : filter.pageSize})
        
        return res;
    }

    static async updateVote(id, data, auth){
        let res = {
            err : false,
            msg : "vote updated successfully"
        }

        let question = await this.findOneQuestion(id);
        if(!question){
            res.err = true;
            res.msg = "Questions not found";
            return res;
        }

        let upVotes = question.upVotes || [];
        let downVotes = question.downVotes || []

        let isVoted = upVotes.find(c=>c == auth._id.toString()) || downVotes.find(c=>c == auth._id.toString())
        if(isVoted){
            res.err = true;
            res.msg = "you already submit a vote";
            return res;
        }
        
        if(data.vote == "up"){
            await this.updateOne(id,{$inc : {votes : 1},$push : {upVotes : auth._id}});
            question.votes+=1;
        }else if(data.vote == "down"){
            await this.updateOne(id,{$inc : {votes : 1},$push : {downVotes : auth._id}});
            question.votes-=1;
        }
        res.data = Mapper.questionVm(question);
        return res;
    }

    static async subscribe(id,auth){
        let res = {
            err : false,
            msg : "subscription enabled"
        }

        let question = await this.findOneQuestion(id);
        if(!question){
            res.err = true;
            res.msg = "Questions not found";
            return res;
        }

        await this.updateOne(question._id,{$push : {subscribers : auth._id}});

        return res;
    }


}

module.exports = QuestionService;