
const UserModel = require('../models/user.model');
const Helper = require('../extensions/helpers.extension');
const Mapper = require('../extensions/mapper.extension');
const RoleService = require('../services/role.service');

class UserService {

    static async create(data){
        return await UserModel.create(data);
    }

    static async updateUser(data){
        return await UserModel.update({_id : data._id},data);
    }

    static async updateMany(data, opt){  
        return await UserModel.updateOne({_id : {$in : data}},opt);  
    }

    static async remove(data){
        return await UserModel.remove(data);
    }

    /**
     * find user
     * @param email email|username
     */
    static async findByUniqueId(email){
        return await UserModel.findOne({$or : [{
            email : email
        },{
            username : email
        }]});
    }

    /**
     * check if email|username|phone already in use
     */
    static async checkIfExist(id){

        return await UserModel.findOne({$or : [{
            email : id
        },{
            username : id
        }]},["email","username","phone"]);
    }






    /**
     * register new users
     * @param {*} data request body
     */
    static async registerUser(data){
        let hash = Helper.hashPassword(data.password);
        let roles = await RoleService.getDefaultRole();

        let user = {
            fullName : data.fullName || "",
            email : data.email,
            userName : data.userName,
            password : hash.hash,
            salt : hash.salt,
            roles : roles
        }
        user = await this.create(user);

        return Mapper.userVm(user);
    }

    static async getBasicProfile(email){
        let user = await this.findByUniqueId(email);
        return Mapper.userVm(user);
    }

    static async updateBasicProfile(update, user){
        user = await this.findByUniqueId(user.email);
        user.fullName = update.fullName;
        user.title = update.title;

        await this.updateUser(user);
        return Mapper.userVm(user);
    }

}

module.exports = UserService;