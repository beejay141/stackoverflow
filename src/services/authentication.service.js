require('dotenv').config();
const Mapper = require('../extensions/mapper.extension');
const {sign} = require('jsonwebtoken');

class AuthenticationService{

    /**
     * generate JWT AuthToken
     * @param {*} user user object
     */
    static generatePayload(user){
        let secret  = process.env.SECRET || "t9u4*&jEsLNdKo&M1A0Cp>Mco`?h.Dz<S`}L:[)UW+w)U^oz!*ROB82|6pyJ,l8";
        let expriry = Number(process.env.EXPIRY) || (60*60*24);
        let data = Mapper.authPayload(user);
        let payload = sign(data,secret,{expiresIn : expriry})
        return {token : payload, exp : expriry,user:data};
    }
}

module.exports = AuthenticationService;