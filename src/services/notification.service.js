const NotificationModel = require('../models/notification.model');
const UserService = require('./user.service');
const Helper = require('../extensions/helpers.extension');
const Mapper = require('../extensions/mapper.extension');

require('dotenv').config();

class NotificationService{
    static async notifyUsers(question , answer){
        let subscribers = question.subscribers || [];
        if(!subscribers.length) return;

        let attr = `${(process.env.url || 'localhost:3000')}/api/v1/questions/${question._id}`;
        let notify = {
            subject: question.title,
            message: answer.body,
            attr : attr,
            users : question.subscribers
        }

        notify = await NotificationModel.create(notify);
        await UserService.updateMany(subscribers,{$push : {notifications : notify._id}});
    }

    static async getUserNotification(filter, auth){

        filter = Helper.setPaging(filter);

        let notifications = await NotificationModel.find({users : {$in : [auth._id]}},["-users"]).skip(filter.skip).limit(filter.pageSize);
        return notifications;
    }
}

module.exports = NotificationService;