const {body,param} = require("express-validator");

class Answervalidator{
    static validateAddAnswer(){
        return [           
            body("body").notEmpty().withMessage("answer body is required"),
            body("questionId").notEmpty().withMessage("question Id is required").isMongoId().withMessage("supply a valid question Id"),   
        ]
    }

    static validateId(){
        return [
            param("id").isMongoId().withMessage("supply a valid id")
        ]
    }
}

module.exports = Answervalidator;