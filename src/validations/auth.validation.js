const { body } = require('express-validator');

class AuthValidation{
    static login(){
        return [
            body('email').notEmpty().withMessage('Email or Username is required'),
            body('password').notEmpty().withMessage('password is required')
        ]
    }
}

module.exports = AuthValidation; 
