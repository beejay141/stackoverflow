const {body} = require('express-validator');
const UserServices = require('../services/user.service');

class AccountValidator {
    static validateCreateUser(){
        return [

            body('fullName').optional().notEmpty().withMessage("name cannot be empty").if((name)=>{
                if(name)return true;
                return false;
            }).isLength({min : 3}).withMessage("name min Lenght is 3"),
            
            body('email').notEmpty().withMessage("Email is required").if((email)=>{
                if(email)return true;
                return false;
            })
            .isEmail().withMessage('Supply a valid email').custom(async(email)=>{
                let user = await UserServices.checkIfExist(email);

                if(user != null)
                    throw new Error("Email already exist");
                
                return true;
            }),

            body('userName').notEmpty().withMessage("username is required").if((email)=>{
                if(email)return true;
                return false;
            }).custom(async(userName)=>{
                let user = await UserServices.checkIfExist(userName);

                if(user != null)
                    throw new Error("username already exist");
                
                return true;
            }),
            
            body('password').notEmpty().withMessage('password, password is required').if((prod)=>{
                if(prod)return true;
                return false;
            }).isLength({min : 6}).withMessage("password, Min. password length is 6")
        ]
    }

    static validateUpdateUser(){
        return [

            body('fullName').optional().notEmpty().withMessage("name cannot be empty").if((name)=>{
                if(name)return true;
                return false;
            }).isLength({min : 3}).withMessage("name min Lenght is 3"),

            body('title').optional().notEmpty().withMessage("title cannot be empty")
        
        ]
    }
}

module.exports = AccountValidator;