const {body,param} = require("express-validator");

class Questionvalidator{
    static validateAddquestion(){
        return [
            body("title").notEmpty().withMessage("title is required"),
            body("body").notEmpty().withMessage("question body is required"),
            body("tags").notEmpty().withMessage("question tag is required").isArray({min:1}).withMessage("minimum of one tag is requires")
        ]
    }

    static validateEditquestion(){
        return [
            body("title").notEmpty().withMessage("title is required"),
            body("body").notEmpty().withMessage("question body is required"),
            body("tags").notEmpty().withMessage("question tag is required").isArray({min:1}).withMessage("minimum of one tag is requires")
        ]
    }

    static validateId(){
        return [
            param("id").isMongoId().withMessage("supply a valid id")
        ]
    }

    static validateVote(){
        return [
            body("vote").notEmpty().withMessage("vote Input is required")
            .custom(c=>{
                if(["up","down"].includes(c)){
                    return true;
                }
                throw new Error("vote must be up|down")
            })
        ]
    }
}

module.exports = Questionvalidator;