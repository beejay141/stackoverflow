const { verify} = require("jsonwebtoken");
require("dotenv").config()

module.exports = {


    key : process.env.SECRET || "t9u4*&jEsLNdKo&M1A0Cp>Mco`?h.Dz<S`}L:[)UW+w)U^oz!*ROB82|6pyJ,l8",
    notAuthMsg : "Session Expired",
    notpermitMsg : "Not Enough Permission",

    CheckAuth(req){
        let res = {
            message : this.notAuthMsg,
            code : 401
        }

        try {

            let btoken = req.headers['authorization'];
            if (!btoken) return res;

            let token = btoken.split(' ')[1];
            let secret = this.key;
            let data = verify(token,secret);
            res.user = data;
            return res;
        } catch (error) {
            return res;
        }
        
    },

    adminAuth(req){

        let res = {
            message : this.notAuthMsg,
            code : 401
        }

        try {
            let btoken = req.headers['authorization'];
            if (!btoken) return res;

            let token = btoken.split(' ')[1];
            let secret = this.key;
            let data = verify(token,secret);
            let adminRole= data.roles.find((c)=>c.name =="admin");

            if(!adminRole){
                res.code = 412,
                res.message = this.notpermitMsg
                return res;
            }
            
            res.user = data;
            return res;
        } catch (error) {
            return res;
        }
    },


    isInAnyOfRoles(user, roles){
        let isRoles = user.roles.filter((c)=>roles.includes(c.name)) || [];
        return isRoles.length > 0;
    }
    
}