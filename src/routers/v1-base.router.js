const express = require('express');
const router = express.Router();
const accountRouter = require('../routers/v1-routers/account.router');
const authRouter = require('../routers/v1-routers/authentication.router');
const questionRouter = require('../routers/v1-routers/question.router');
const ansRouter = require('../routers/v1-routers/answer.router');
const notificationRouter = require('../routers/v1-routers/notification.router');


router.use('/auths', authRouter);
router.use('/accounts', accountRouter);
router.use('/questions', questionRouter);
router.use('/answers', ansRouter);
router.use('/notifications', notificationRouter);


router.use(function(req, res, next) {
    return res.status(404).send({ app_name : "Stack Overflow", version : "v1" });
});



module.exports = router;