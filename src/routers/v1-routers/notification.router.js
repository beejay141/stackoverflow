const express = require("express");
const NotificationController = require('../../controllers/notification.controller');


const router = express.Router()


router.get('/', NotificationController.getUserNotification);



// for unmapped routes
router.use(function(req, res, next) {
    return res.status(404).send({
        App : "Easy coop API",
        Version : "1.0.0"
    });
});

module.exports = router;