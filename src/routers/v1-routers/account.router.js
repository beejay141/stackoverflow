const express = require("express");
const AccountValidator = require('../../validations/account.validator');
const AccountController = require('../../controllers/account.controller');


const router = express.Router()


router.post('/register',AccountValidator.validateCreateUser() , AccountController.Register);

router.put('/',AccountValidator.validateUpdateUser() , AccountController.updateBasicProfile);

router.get('/' , AccountController.getBasicProfile);



// for unmapped routes
router.use(function(req, res, next) {
    return res.status(404).send({
        App : "Easy coop API",
        Version : "1.0.0"
    });
});

module.exports = router;