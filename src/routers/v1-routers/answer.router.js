const express = require("express");
const AnswerValidator = require('../../validations/answer.validation');
const AnswerController = require('../../controllers/answer.controller');


const router = express.Router()


router.post('/',AnswerValidator.validateAddAnswer() , AnswerController.SubmitAnswer);




// for unmapped routes
router.use(function(req, res, next) {
    return res.status(404).send({
        App : "Easy coop API",
        Version : "1.0.0"
    });
});

module.exports = router;