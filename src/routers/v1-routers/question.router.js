const express = require("express");
const QuestionValidator = require('../../validations/question.validator');
const QuestionController = require('../../controllers/question.controller');


const router = express.Router()


router.post('/',QuestionValidator.validateAddquestion() , QuestionController.postQuestion);

router.put('/subscribe/:id', QuestionValidator.validateId(),QuestionController.updateSubscribe);
router.put('/votes/:id', QuestionValidator.validateId(), QuestionValidator.validateVote(),QuestionController.updateVote);
router.put('/:id', QuestionValidator.validateId(), QuestionValidator.validateEditquestion(),QuestionController.EditQuestion);

router.get('/:id' , QuestionController.getQuestion);
router.get('/' , QuestionController.getQuestions);



// for unmapped routes
router.use(function(req, res, next) {
    return res.status(404).send({
        App : "Easy coop API",
        Version : "1.0.0"
    });
});

module.exports = router;