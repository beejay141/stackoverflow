const express = require("express");
const AuthValidator = require('../../validations/auth.validation');
const AuthController = require('../../controllers/authentication.controller');


const router = express.Router()


router.post('/login',AuthValidator.login() , AuthController.Login);

// todo
// router.post('/social' , AuthController.SocialLogin);



// for unmapped routes
router.use(function(req, res, next) {
    return res.status(404).send({
        App : "Easy coop API",
        Version : "1.0.0"
    });
});

module.exports = router;