const mongoose = require("../dbs/mongoose.db");

const answerSchema = new mongoose.Schema({
    question : {type : mongoose.Schema.Types.ObjectId, ref : "Question"},
    questionTitle : String,
    user : {type : mongoose.Schema.Types.ObjectId,ref : "User"},
    userName : String,
    body : String,
    
    votes : {type : Number, default : 0},
    upVotes : Array,
    downVotes : Array,

    comments : [
        {
            body: String,
            user: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "User"
            },
            userName: String,
            date: Date,
            flags: [{
                    comment: String,
                    comType: String,
                    date: Date
                }]
        }
    ],

    editedBy : {type : mongoose.Schema.Types.ObjectId,ref : "User"},
    editedByUserName : String,
    editedOn : Date,
    
    deleted : Boolean

},{timestamps : true});

const AnswerModel = mongoose.model('Answer',answerSchema);

module.exports = AnswerModel;