const mongoose = require("../dbs/mongoose.db");

const questionSchema = new mongoose.Schema({
    user : {type: mongoose.Schema.Types.ObjectId, ref:'User'},
    userName : String,

    title : String,
    body : String,

    votes : {type : Number, default : 0},
    upVotes : Array,
    downVotes : Array,

    tags : {type : Array, default : []},
    
    answers : [{type : mongoose.Schema.Types.ObjectId,  ref: 'Answer'}],
    answersCount : {type : Number, default : 0},

    subscribers : [{type : mongoose.Schema.Types.ObjectId,ref: 'User'}],
    subscribersCount : {type : Number, default : 0},

    viewedBy : [],
    viewedCount : {type : Number, default : 0},

    editedBy : {type : mongoose.Schema.Types.ObjectId,ref : "User"},
    editedByUserName : String,
    editedOn : Date,

    deleted : Boolean

},{timestamps : true});

const QuestionModel = mongoose.model('Question',questionSchema);

module.exports = QuestionModel;