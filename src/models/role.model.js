const mongoose = require("../dbs/mongoose.db");

const roleSchema = new mongoose.Schema({
    name : String,
    displayName : String
});

const RoleModel = mongoose.model('Role',roleSchema);

module.exports =  RoleModel;