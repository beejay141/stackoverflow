const mongoose = require("../dbs/mongoose.db");

const TagSchema = new mongoose.Schema({
    name: String,
    description : String,

    watchers : [{type : mongoose.Schema.Types.ObjectId, ref : 'User' }],
    questions : [{type : mongoose.Schema.Types.ObjectId, ref : 'Questions' }]

},{timestamps : true});

const TagModel = mongoose.model('Tag',TagSchema);

module.exports = TagModel;