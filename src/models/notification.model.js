const mongoose = require("../dbs/mongoose.db");

const notificationSchema = new mongoose.Schema({
    subject : String,
    message : String,
    attr : String,
    users : [{type : mongoose.Schema.Types.ObjectId, ref : 'User'}]

},{timestamps : true});

const notificationModel = mongoose.model('Notification',notificationSchema);

module.exports =  notificationModel;