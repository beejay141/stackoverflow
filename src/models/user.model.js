const mongoose = require("../dbs/mongoose.db");

const userSchema = new mongoose.Schema({
    fullName : {type : String, default : ""},
    email : {type : String, required : true, unique : true},
    userName : String,
    password : String,
    salt : String,
    title : {type : String, default : ""},

    reputation : {type : Number, default : 0},

    roles : Array,

    notifications : [{type : mongoose.Schema.Types.ObjectId,ref: 'Notification'}]

},{timestamps : true});

const UserModel = mongoose.model('User',userSchema);

module.exports = UserModel;