const RoleService = require('../../services/role.service');
const UserService = require('../../services/user.service');
const Helper = require('../../extensions/helpers.extension');
const TagService = require('../../services/tag.service');

class InitSeeder { 

    constructor(){

        this.createRoles().then(data=>{

            this.createAdmin(data).then()
            .catch(err=>{
                Helper.logError(err);
            })
            
        }).catch(err=>{
            Helper.logError(err);
        })

        this.addSomeTags().then(data=>{

            Helper.logInfo(data);
            
        }).catch(err=>{
            Helper.logError(err);
        })

    }


    async createRoles(){
        let roles = [
            {name : "user",displayName:"User"},
            {name : "admin",displayName : "Administrator"}
        ]

        await RoleService.remove({});

        roles = await RoleService.CreateRole(roles);

        Helper.logInfo(`role seeded`);
        Helper.logInfo(JSON.stringify(roles));

        return roles;
    }

    async createAdmin(roles){
        let hash = Helper.hashPassword("admin");
        let user = {
            email : "admin@gmail.com",
            password : hash.hash,
            salt : hash.salt,
            roles : roles
        };

        await UserService.remove({});
        user = await UserService.create(user);
        
        Helper.logInfo(`admin user seeded`);
        Helper.logInfo(JSON.stringify(user));
    }

    async addSomeTags(){
        let tags = [
            {name : "nodeJs",description : "Node.js is an event-based, non-blocking, asynchronous I/O runtime that uses Google's V8 JavaScript engine and libuv library"},
            {name : "C#",description : "JavaScript (not to be confused with Java) is a high-level, dynamic, multi-paradigm, object-oriented, prototype-based, weakly-typed, and interpreted language used for both client-side and server-side scripting"}
        ]

        await TagService.remove({});

        return await TagService.create(tags);
    }
}

new InitSeeder();