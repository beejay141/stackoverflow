require('dotenv').config();
const mongoose = require('mongoose');

const {
    databaseDriver,
    databaseHost,
    databasePort,
    databaseUser,
    databasePwd,
    databaseCollection,
  } = {
      databaseCollection: process.env.DATABASE_COLLECTION || 'stackdb',
      databaseDriver: process.env.DATABASE_DRIVER || 'mongodb',
      databaseHost: process.env.DATABASE_HOST || 'localhost',
      databasePort: process.env.DATABASE_PORT || '27017',
      databaseUser: process.env.DATABASE_USER || 'stack-user',
      databasePwd: process.env.DATABASE_PWD || 'stack-pwd',
  }
  
  mongoose.Promise = global.Promise;
  mongoose.connect(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize : 7,
 },(err)=>{
   if(err)
    console.log("db cconnection failed", err)
    else
    console.log(`db connected`)
 });

 module.exports = mongoose;
