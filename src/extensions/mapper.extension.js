const dash = require("lodash");

class Mapper {

    static userVm(user) {

        return dash.pick(user, "fullName", "email", "userName","title","reputation","roles", "createdAt", "updatedAt")
    }

    static authPayload(user) {
        return dash.pick(user, "_id", "userName", "email","title","roles")
    }

    static questionVm(question = null){
        let selected = ["_id","userName","title","body","votes","tags","answers","subscribersCount","viewedCount","editedByUserName","editedOn"]
        if(!question){
            return selected
        }
        return dash.pick(question,selected);
    }

    static notifyVm(notify = null){
        let selected = ["_id","subject","message","attr"];
        if(!notify) return selected;

        return dash.pick(notify,selected);
    }
}

module.exports = Mapper;