const express = require("express");
const { validationResult } = require("express-validator");
const bcrypt = require('bcryptjs');

class Helper{

    static errorFormatter(errors) {
        let errs = [];

        errors.forEach(e => {
            errs.push(`${e.msg}`)
        });
        return errs;
        
    }

    
    static logServerError(req , error , dbLog = false){
        console.error(`Server Error query ${JSON.stringify(req.query)} \n param : ${JSON.stringify(req.param)} \n body : ${JSON.stringify(req.body)} \n ${error}`);
    
    }

    static logError(message , dbLog = false){
        console.log(message,new Date());
    }

    static logInfo(message , dbLog = false){
        console.log(message,new Date());
    }

    /**
     * 
     * @param {express.Request} req 
     */
    static validateInputData(req){
        let errors = validationResult(req);
            if(!errors.isEmpty()){
              return errors.array()
            }
            return false;
    }

    static generateRamdom(size = 4, alpha = true, referralCode=false){
        const pool = alpha ? 'ABCDEFGHIJKLMNPQRSTUVWXYZ0123456789abcdefghijklmnpqrstuvwxyz' : '0123456789';
        const rands = []; let i = -1;
    
        while (++i < size) rands.push(pool.charAt(Math.floor(Math.random() * pool.length)));

        if(referralCode){
            rands.push('-')
            i=-1;
            while (++i < size) rands.push(pool.charAt(Math.floor(Math.random() * pool.length)));
        }
    
        return rands.join('');
    }

    static setPaging(filter){
        filter.pageSize = Number(filter.pagesize) || Number(filter.pageSize) || 50 ;
        filter.page = Number(filter.page) || 1;
        filter.skip = (filter.page - 1 ) * filter.pageSize;
        return filter;
    }

    static getSlug(data){
        let slug = data;
        while(slug.includes(" ")){
            slug = slug.replace(" ","-")
        }
        return slug;
    }

    static hashPassword(password , salt = null){
        if(!salt)
            salt = bcrypt.genSaltSync(10);

        let hash = bcrypt.hashSync(password,salt);
        return {salt : salt,hash : hash}
    }

    /**
     * compare hashed password with the plain
     * @param {*} hashedPassword 
     * @param {*} plainPassword 
     */
    static comparePassword(hashedPassword, plainPassword){
        return bcrypt.compareSync(plainPassword,hashedPassword);
    }
    
}

module.exports = Helper;