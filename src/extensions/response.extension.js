const Helper = require('./helpers.extension');

function Response(err = false, message = "", data = null) {
  if (err) {
    return {
      error : true,
      message: typeof message == 'object' ? "error" : message,
      errors: typeof message == 'object' ? Helper.errorFormatter(message) : [message],
    }
  } else {
    return {
      error : false,
      message: message,
      data: data
    }
  }
}


module.exports = Response;